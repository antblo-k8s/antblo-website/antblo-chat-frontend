FROM node:14.14.0-buster AS base
RUN npm install -g @angular/cli
RUN npm install --save-dev @angular-devkit/build-angular

FROM base AS debug
WORKDIR /app
RUN apt update -y && apt install curl iptables sudo sshfs -y
RUN sudo curl -fL https://app.getambassador.io/download/tel2/linux/amd64/latest/telepresence -o /usr/local/bin/telepresence
RUN sudo chmod a+x /usr/local/bin/telepresence

FROM base AS build
WORKDIR /app
COPY . .
RUN npm install
RUN ng build --prod

FROM node:14.14.0-buster-slim AS final
WORKDIR /app
COPY --from=build /app/dist /app/dist
RUN npm install -g serve
EXPOSE 5000
CMD serve /app/dist/antblo-chat
