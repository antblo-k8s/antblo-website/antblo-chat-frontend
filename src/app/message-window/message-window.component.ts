import { Component, OnInit } from '@angular/core';
import { Message, ChatService } from '../chat.service';


@Component({
  selector: 'app-message-window',
  templateUrl: './message-window.component.html',
  styleUrls: ['./message-window.component.scss']
})
export class MessageWindowComponent implements OnInit {
  messages: Array<Message>;

  constructor(messageService: ChatService) {
    this.messages = messageService.messages;
  }

  ngOnInit(): void {
  }
}
