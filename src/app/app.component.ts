import { Component, OnInit } from '@angular/core';
import { ChatService } from './chat.service';
import { LoginService } from './login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  chatService: ChatService;
  loginService: LoginService;

  constructor(chatService: ChatService, loginService: LoginService) {
    this.chatService = chatService;
    this.loginService = loginService;
    setTimeout(() => {
    }, 3000)
    this.loginService.onLogin(async () => {
      let token = this.loginService.token;
      await this.chatService.start(token);
    })
  }

  login() {
    this.loginService.login()
  }
}