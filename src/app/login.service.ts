import { Injectable } from '@angular/core';
import * as Keycloak from 'keycloak-js';
import { parse } from 'tldts';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public get token(): string {
    if (this.keycloak.token) {
      return this.keycloak.token;
    }
    else {
      console.error("No token exists.");
      return "";
    }
  }
  private redirectUri: string;
  private url: string;
  private keycloak: Keycloak.KeycloakInstance;

  public get isLoggedIn() {
    return this.keycloak.authenticated
  }

  login() {
    this.keycloak.login();
  }

  onLogin(callback: Function) {
    this.keycloak.onAuthSuccess = async () => {
      callback();
    }
  }

  constructor() {
    let parsedUrl = parse(location.hostname);
    this.url = `https://keycloak.${parsedUrl.domainWithoutSuffix}.${parsedUrl.publicSuffix}/auth`;
    this.redirectUri = `https://chat.${parsedUrl.domainWithoutSuffix}.${parsedUrl.publicSuffix}/`;

    let config: Keycloak.KeycloakConfig = {
      url: this.url,
      realm: 'antblo',
      clientId: 'chat',
    };
    let keycloak = Keycloak(config);

    keycloak.init({
      redirectUri: this.redirectUri
    })
    this.keycloak = keycloak;
  }
}
