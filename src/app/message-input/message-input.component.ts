import { Component, OnInit } from '@angular/core';
import { ChatService } from '../chat.service';

@Component({
  selector: 'app-message-input',
  templateUrl: './message-input.component.html',
  styleUrls: ['./message-input.component.scss']
})
export class MessageInputComponent implements OnInit {
  chatService: ChatService;

  constructor(chatService: ChatService) {
    this.chatService = chatService
  }

  ngOnInit(): void {
  }

  send(msgTextArea: HTMLTextAreaElement) {
    this.chatService.sendMessage(msgTextArea.value);
    msgTextArea.value = ""
  }
}
