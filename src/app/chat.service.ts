import { Injectable } from '@angular/core';
import * as signalR from "@microsoft/signalr";
import { HubConnection } from '@microsoft/signalr';
import jwtDecode from 'jwt-decode';

export type Message = { username: string, text: string }
export type ChatState = { email: string | null, isLoggedIn: boolean }

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  messages: Array<Message> = [];
  connection: HubConnection;
  username: string | null = null;

  constructor() {
    this.connection = new signalR.HubConnectionBuilder()
      .withUrl("/hub", {
        skipNegotiation: true,
        transport: signalR.HttpTransportType.WebSockets
      })
      .build();
    this.connection.on("ReceiveMessage", (username, message) => {
      this.messages.unshift({ username: username, text: message });
    });
  }

  async sendMessage(message: string) {
    if (this.username != null) {
      this.connection.send("SendMessage", message);
    }
  }

  async start(token: string) {
    let parsedToken: any = jwtDecode(token);
    this.username = parsedToken["preferred_username"];
    await this.connection.start();
    await this.connection.send("Authorization", token);
  }
}
